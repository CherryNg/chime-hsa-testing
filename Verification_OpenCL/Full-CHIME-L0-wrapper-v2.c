//gcc -std=gnu99 -O3 -Wall Full-CHIME-L0-wrapper.c -I$AMDAPPSDKROOT/include -lOpenCL -lm -L$AMDAPPSDKROOT/lib/x86_64/ -o Full-CHIME-L0-wrapper

 #include <stdio.h>
 #include <stdlib.h>
 #include <math.h>
 #include <unistd.h>
 #include <CL/cl.h>
 #include <sys/time.h>

 #define NUM_CL_FILES        3
 #define OPENCL_FILENAME_1   "unpack_shift_beaform.cl"
 #define OPENCL_FILENAME_2   "transpose.cl"
 #define OPENCL_FILENAME_3   "Upchannelize.cl"
 #define N_QUEUES            1
 #define PI 3.14159265
 #define feed_sep 0.3048
 #define light 3.e8
 #define Freq_ref 492.125984252
 
double e_time(void){
  static struct timeval now;
  gettimeofday(&now, NULL);
  return (double)(now.tv_sec  + now.tv_usec/1000000.0);
}

void clamping(int *map_index, float freq, int nbeamsNS)
{
  float t, delta_t, Beam_Ref;
  int cl_index;
  float D2R = PI/180.;
  int pad = 2 ;
  int tile=1;
  for (int b=0;b<nbeamsNS; b++){
    Beam_Ref = asin(light*(b-nbeamsNS/2.) / (Freq_ref*1.e6) / (nbeamsNS) /feed_sep) * 180./ PI;
    t = nbeamsNS*pad*(Freq_ref*1.e6)*(feed_sep/light*sin(Beam_Ref*D2R)) + 0.5;
    delta_t = nbeamsNS*pad*(freq*1e6-Freq_ref*1e6) * (feed_sep/light*sin(Beam_Ref*D2R));
    cl_index = (int) floor(t + delta_t) + nbeamsNS*tile*pad/2.;
    if (cl_index < 0)
      cl_index = nbeamsNS*tile*pad + cl_index;
    else if (cl_index > nbeamsNS*tile*pad)
      cl_index = cl_index - nbeamsNS*tile*pad;
    cl_index = cl_index - 256;
    if (cl_index < 0){
      cl_index = nbeamsNS*tile*pad + cl_index;
    }
    map_index[b] = cl_index;
  }
}


unsigned char * dataload(const char* filename){//, int* arraysize){
   
  FILE *f_handle;
  unsigned char *f_buffer;
  size_t f_size;
  int err;
    
  f_handle = fopen(filename, "r");
  if(f_handle == NULL) {perror("Couldn't find the program int file");exit(1);}
  fseek(f_handle, 0, SEEK_END);
  f_size = ftell(f_handle);
  rewind(f_handle);
  printf("In dataload, allocating %d bytes \n", f_size );
  f_buffer = (unsigned char*)malloc(f_size);
  //  f_buffer[f_size] = '\0';
  fread(f_buffer, sizeof(unsigned char), f_size, f_handle);
  fclose(f_handle);
    
  //  *arraysize = f_size/4;
  return f_buffer;
    
}  


// int main( void )
int main ( int argc )
 {
   cl_int err;
   cl_platform_id platform = 0;
   cl_device_id device = 0;
   cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
   cl_context ctx = 0;
   cl_command_queue queue = 0;
   int ret = 0;
   double elapsed_time;
   char platform_name[128];
   char device_name[128];
   cl_ulong maxbuffersize;
   float FREQ1 = 450.0;

   /* Setup OpenCL environment. */
   //Platform
   err = clGetPlatformIDs( 1, &platform, NULL );
   size_t ret_param_size = 0;
   err = clGetPlatformInfo(platform, CL_PLATFORM_NAME,
			   sizeof(platform_name), platform_name,
			   &ret_param_size);
   printf("Platform found: %s\n", platform_name);


   //GPU device
   err = clGetDeviceIDs( platform, CL_DEVICE_TYPE_DEFAULT, 1, &device, NULL );
   err = clGetDeviceInfo(device, CL_DEVICE_NAME,
			 sizeof(device_name), device_name,
			 &ret_param_size);
   err = clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
			 sizeof(maxbuffersize), &maxbuffersize, NULL);

   printf("Device found on the above platform: %s; max const. buffer %lu\n", device_name, maxbuffersize);

   //Context
   props[1] = (cl_context_properties)platform;
   ctx = clCreateContext( props, 1, &device, NULL, NULL, &err );
   queue = clCreateCommandQueue( ctx, device, 0, &err );

   //load program
   char cl_fileNames[3][256];
   sprintf(cl_fileNames[0],OPENCL_FILENAME_1);
   sprintf(cl_fileNames[1],OPENCL_FILENAME_2);
   sprintf(cl_fileNames[2],OPENCL_FILENAME_3);

   char cl_options[1024]= "-cl-mad-enable";
   size_t cl_programSize[NUM_CL_FILES];
   FILE *fp;
   char *cl_programBuffer[NUM_CL_FILES];

   for (int i = 0; i < NUM_CL_FILES; i++){
     fp = fopen(cl_fileNames[i], "r");
     if (fp == NULL){
       printf("error loading file: %s\n", cl_fileNames[i]);
       return (-1);
     }
     fseek(fp, 0, SEEK_END);
     cl_programSize[i] = ftell(fp);
     rewind(fp);
     cl_programBuffer[i] = (char*)malloc(cl_programSize[i]+1);
     cl_programBuffer[i][cl_programSize[i]] = '\0';
     int sizeRead = fread(cl_programBuffer[i], sizeof(char), cl_programSize\
			  [i], fp);
     if (sizeRead < cl_programSize[i])
       printf("Error reading the file!!!");
     fclose(fp);
   }

   cl_program program = clCreateProgramWithSource( ctx, NUM_CL_FILES, (const char**)cl_programBuffer, cl_programSize, &err );
   if (err){
     printf("Error in clCreateProgramWithSource: %i\n",err);
     return(-1);
   }

   size_t log_size;
   char *program_log;
   err = clBuildProgram( program, 1, &device, cl_options, NULL, NULL );
   if (err != 0){
     printf("Error in clBuildProgram: %i\n",err);
     clGetProgramBuildInfo (program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
     program_log = (char*) calloc(log_size+1, sizeof(char));
     clGetProgramBuildInfo (program, device, CL_PROGRAM_BUILD_LOG, log_size+1, program_log, NULL);
     printf("%s\n", program_log);
     free(program_log);
     return(-1);
   }
   printf("Creating kernels---\n");
   cl_kernel beamform = clCreateKernel( program, "zero_padded_FFT512", &err );
   if (err){
     printf("Error in clCreateKernel 1: %i\n",err);
     return (-1);
   }
   cl_kernel transpose = clCreateKernel( program, "transpose", &err );
   if (err){
     printf("Error in clCreateKernel 2: %i\n",err);
     return (-1);
   }

   cl_kernel Upchannelize = clCreateKernel( program, "Upchannelize", &err );
   if (err){
     printf("Error in clCreateKernel 3: %i\n",err);
     return (-1);
   }

   for (int i =0; i < NUM_CL_FILES; i++){
     free(cl_programBuffer[i]);
   }

   //Parameters, maybe better off in a config file++++++++++++++++++++++++++++++++++++++++
   int nfreq_up = 128 ;
   int t_ds = 3;  //time downsample factor
   int nsamp_in = 100*nfreq_up*t_ds;  //this is the number of samples unpacked in floats
   int nsamp_in_packed = nsamp_in; // packed, 4 uchar = 1 uint = 4pairs(Re+Im)
   int nsamp_out = (nsamp_in/nfreq_up)/t_ds;
   int nbeamsNS = 256;
   int nbeamsEW = 4;
   int nbeams = nbeamsEW*nbeamsNS; //1024
   int n_elm = 2048;  //1024feeds * 2pol
   int npol = 2;
   int nfreq_out = 16;
   int N_Raw = nsamp_in_packed*n_elm;  
   int N_Unpack = nsamp_in*n_elm*2;  //factor of 2 for Re+Im
   int N_Out = nfreq_out*nsamp_out*nbeams;

   //Simulate InputRaw: dimension time-pol-elm_NS-elm_EW with time the slowest varying;
   printf("\nAllocating Raw Input (4 bit packed integer)\n");
   unsigned char *InputRaw;
   //   InputRaw =(unsigned char *) malloc(N_Raw); 
   InputRaw =(unsigned char *) calloc(N_Raw, sizeof(unsigned char));
   if (InputRaw == NULL){
     printf("Error allocating memory for InputRaw.");
   }

   unsigned char temp_output;
   srand(42);
   for (int i=0; i<N_Raw; i++) {
     unsigned char new_real;
     unsigned char new_imaginary;
     new_real = rand()%16;  //to put the pseudorandom value in the range 0-15
     new_imaginary = rand()%16;
     temp_output = ((new_real<<4) & 0xF0) + (new_imaginary & 0x0F);
     InputRaw[i]= temp_output;
   }

   for (int i=0;i<3;i++){
     for (int j=0;j<3;j++){
       printf("%d  ", InputRaw[(i*2048+j)]);
     }
     printf("\n");
   }
   printf("....\n");
   for(int i=nsamp_in-2;i<nsamp_in+1;i++){
     for (int j=3;j>0;j--){
       printf("%d  ", InputRaw[(i*2048-j)]);
     }
     printf("\n");
   }

   float *InputUnpacked; //using calloc, so no need initialize zeros?
   InputUnpacked =(float *) calloc(N_Unpack , sizeof(float));
   if (InputUnpacked == NULL){
     printf("Error allocating memory for InputUnpacked.");
   }

   float *data_transposed; //using calloc, so no need initialize zeros?
   data_transposed =(float *) calloc(N_Unpack, sizeof(float));
   if (data_transposed == NULL){
     printf("Error allocating memory for data_transposed.");
   }

   float *Z; //using calloc, so no need initialize zeros?
   Z =(float *) calloc(N_Unpack , sizeof(float));
   if (Z == NULL){
     printf("Error allocating memory for Z (output).");
   }


   float *Co;
   Co =(float *) malloc(16*2*sizeof(float));
   if (Co == NULL){
     printf("Error allocating memory for Co.");
   }
   //Initiate coefficients to be the same
   for (int angle_iter=0; angle_iter < 4; angle_iter++){
     float anglefrac = sin(0.4*angle_iter*PI/180.);   //say 0, 0.4, 0.8 and 1.2 for now. 
     for (int cylinder=0; cylinder < 4; cylinder++){
       Co[angle_iter*4*2 + cylinder*2] = cos(2*PI*anglefrac*cylinder*22*FREQ1*1.e6/light);
       Co[angle_iter*4*2 + cylinder*2 + 1] = sin(2*PI*anglefrac*cylinder*22*FREQ1*1.e6/light);
     }
   }

   //These are the desired 256 beams to be selected from the 512 fft output
   int mapped_output[256];
   clamping(mapped_output, FREQ1, nbeamsNS);

   /* Prepare OpenCL memory objects and place data inside them. */
   cl_mem bufX, bufY, bufZ, buf_transposed, device_mapped_output, bufCo;
   bufX = clCreateBuffer( ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, N_Raw*sizeof(unsigned char), InputRaw, &err );

   bufY = clCreateBuffer( ctx, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, N_Unpack*sizeof(float), InputUnpacked, NULL);
   err = clEnqueueWriteBuffer( queue, bufY, CL_TRUE, 0, sizeof(float)*N_Unpack, InputUnpacked, 0, NULL, NULL );

   bufZ = clCreateBuffer( ctx, CL_MEM_WRITE_ONLY, N_Unpack*sizeof(float), NULL, NULL );
   err = clEnqueueWriteBuffer( queue, bufZ, CL_TRUE, 0, sizeof(float)*N_Unpack, Z, 0, NULL, NULL );

   buf_transposed = clCreateBuffer( ctx, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, N_Unpack*sizeof(float), data_transposed, NULL );
   err = clEnqueueWriteBuffer( queue, buf_transposed, CL_TRUE, 0, sizeof(float)*N_Unpack, data_transposed, 0, NULL, NULL );

   device_mapped_output = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 256*sizeof(int), mapped_output, &err);
   bufCo = clCreateBuffer( ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float)*16*2, Co, &err );

  //start the timer
  elapsed_time = e_time();

  //Step 1: unpack - beamformNS - beamformEW
  size_t global_size[] = { 256, 2, nsamp_in};
  size_t local_size[] =  { 256, 1, 1};
  clSetKernelArg(beamform, 0, sizeof(cl_mem), &bufX);       //input
  clSetKernelArg(beamform, 1, sizeof (void *), (void *) &device_mapped_output);  
  clSetKernelArg(beamform, 2, sizeof(cl_mem), &bufCo);
  clSetKernelArg(beamform, 3, sizeof(cl_mem), &bufY);      //output


  //Step 2: transpose 
  int size = nsamp_in/4;
  size_t global_size2[] = { n_elm, size};
  size_t local_size2[] =  { 32,8};
  //supposedly transpose data from time-pol-beams to pol-beam-time
  clSetKernelArg(transpose, 0, sizeof(cl_mem), &bufY);   //use output from previous as input
  clSetKernelArg(transpose, 1, sizeof(cl_mem), &buf_transposed);   //output


  //Step 3: Upchannelization
  size_t global_size3[] = { size*npol*nbeams, 1};
  size_t local_size3[] =  { 64,1};
  clSetKernelArg(Upchannelize, 0, sizeof(cl_mem), &buf_transposed);   
  clSetKernelArg(Upchannelize, 1, sizeof(cl_mem), &bufZ);   //output

  //Enqueue
  cl_event Event1, Event2, Event3;

  int extra_coef = 1;
  for (int i = 0; i < extra_coef; i++){
    clEnqueueNDRangeKernel(queue,
			   beamform,
			   3,
			   NULL,
			   global_size,
			   local_size,
			   0,    //number of things waiting on 
			   NULL,   //wait on this
			   &Event1);  //flag this when done
    
   
    //printf("Step 2) beamform NS\n");
    clEnqueueNDRangeKernel(queue,
			   transpose,
			   2,
			   NULL,
			   global_size2,
			   local_size2,
			   1,         //number of things it is waiting on
			   &Event1,   //wait on this
			   &Event2);  //flag this when done
    
    clReleaseEvent(Event1);    
    
    //printf("Step 3) Upchannelise\n");
    clEnqueueNDRangeKernel(queue,
			   Upchannelize,
			   2,
			   NULL,
			   global_size3,
			   local_size3,
			   1,         //number of things it is waiting on
			   &Event2,   //wait on this
			   &Event3);  //flag this when done
    clReleaseEvent(Event2);    

    // Wait for calculations to be finished.
    err = clFinish(queue);
  }
  //stop the timer
  elapsed_time = e_time() - elapsed_time;
  //Fetch results
  err = clEnqueueReadBuffer( queue, bufY, CL_TRUE, 0, sizeof(float)*N_Unpack, InputUnpacked, 1, &Event3, NULL );
  err = clEnqueueReadBuffer( queue, buf_transposed, CL_TRUE, 0, sizeof(float)*N_Unpack, data_transposed, 1, &Event3, NULL );
  err = clEnqueueReadBuffer( queue, bufZ, CL_TRUE, 0, sizeof(float)*N_Unpack, Z, 1, &Event3, NULL );

  printf("\n====THE END ====\n");

  //InputUnpacked: [time - pol - beamNS - beamEW]
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){ 
      printf("(%.2f %.2f) ", InputUnpacked[(i*(2048)+j)*2], InputUnpacked[(i*(2048)+j)*2+1]);
    }
    printf("\n");
  }
  printf("...\n");
  for (int i=32760;i<32769;i++){
    for (int j =3;j>0;j--){
      printf("(%.2f %.2f) ", InputUnpacked[(i*(2048)-(j))*2], InputUnpacked[(i*(2048)-(j))*2+1]);
    }
    printf("\n");
  }

  printf("...\n");
  for (int i=nsamp_in-3;i<nsamp_in+1;i++){
    for (int j =3;j>0;j--){
      printf("(%.2f %.2f) ", InputUnpacked[(i*(2048)-(j))*2], InputUnpacked[(i*(2048)-(j))*2+1]);
    }
    printf("\n");
  }

  printf(".....Output of transpose:\n");
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      printf("(%.2f %.2f) ", data_transposed[(i*nsamp_in+j)*2], data_transposed[(i*nsamp_in+j)*2+1]);
    }
    printf("\n");
  }
  printf("...\n");
  for (int i=2048-3;i<2048+1;i++){
    for (int j =3;j>0;j--){
      printf("(%.2f %.2f) ", data_transposed[(i*nsamp_in-j)*2], data_transposed[(i*nsamp_in-j)*2+1]);
    }
    printf("\n");
  }
  printf("... Output of upchannelization:\n");
  for (int b=0;b<3;b++){
    printf("Beam:%d\n",b);
    for (int t=0;t<3;t++){
      for (int f=0;f<3;f++){
	printf(" %.2f ", Z[b*100*16 + t*16 + f]);
      }
      printf("\n");
    }
  }
  printf(".....\n");
  for (int b=1021;b<1028;b++){
    printf("Beam:%d\n",b);
    for (int t=96;t<100;t++){
      for (int f=13;f<16;f++){
	printf(" %.2f ", Z[b*100*16 + t*16 + f]);
      }
      printf("\n");
    }
  }
  printf(".....\n");
  for (int b=2045;b<2048;b++){
    printf("Beam:%d\n",b);
    for (int t=96;t<100;t++){
      for (int f=13;f<16;f++){
	printf(" %.2f ", Z[b*100*16 + t*16 + f]);
      }
      printf("\n");
    }
  }

  /*  for (int b=0;b<2048;b++){
    printf("Beam:%d first: %.2f\n",b, Z[b*100*16]);
    }*/
    


  //clReleaseEvent(Event1);  
  //clReleaseEvent(Event2);
  clReleaseEvent(Event3);

  /* Release OpenCL memory objects. */
  clReleaseMemObject( bufX );
  clReleaseMemObject( bufY );
  clReleaseMemObject( bufZ );
  clReleaseMemObject( bufCo );
  clReleaseMemObject( buf_transposed );
  clReleaseMemObject(device_mapped_output);

  //free arrays
  free(InputRaw);
  free(InputUnpacked);
  free(Z);
  free(Co);
  free(data_transposed);

  // Release kernel
  clReleaseKernel(beamform);
  clReleaseKernel(transpose);
  clReleaseKernel(Upchannelize);

  /* Release OpenCL working objects. */
  clReleaseCommandQueue( queue );
  clReleaseContext( ctx );

  return ret;
}
