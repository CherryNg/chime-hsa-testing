#pragma once
#include "HSA_kernel.hpp"
#define N_ELEM 2048
#define N_ITER 32768
#define N_PRESUM 1024

class HSA_kernel_presum : public HSA_kernel {
	public:
		HSA_kernel_presum(HSA_device *device);
		~HSA_kernel_presum();
		void enqueue(hsa_signal_t*);

		int copy_output_data();
		int verify_output(int spotcheck = 0);
		void allocate_hsa_memory(void **);
		void initialize_memory();
		void **get_hsa_memory();

		uint32_t *presum;
		char *input;

	    unsigned int* input_buffer;
	    unsigned int* presum_buffer;
	    void *kernarg_address;
};
