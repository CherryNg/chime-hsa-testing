#pragma once
#include <list>
#include <cassert>
#include <sys/time.h>
#include <unistd.h>

#include "hsa/hsa.h"
#include "hsa/hsa_ext_finalize.h"
#include "hsa/hsa_ext_amd.h"
#include "HSA_kernel_presum.hpp"
#include "HSA_kernel_N2.hpp"
#include "HSA_kernel_beamform.hpp"
#include "HSA_kernel_transpose.hpp"
#include "HSA_kernel_upchan.hpp"
#include "HSA_device.hpp"

class HSA_factory{
    public:
        hsa_status_t hsa_status;
        HSA_factory(int num_gpus);
        ~HSA_factory();
        int enqueue();
        int check();

        unsigned char *input_data;
        int *presum;    //output of presum kernel, copied to presum_buffer, used in verify
        float *beamformed_data;  // Do I need this? yes
        float *transposed_data;  // Do I need this? yes
        int num_gpus;

    private:
        std::list<HSA_kernel*> kernels;
        std::list<HSA_device*> devices;

};
