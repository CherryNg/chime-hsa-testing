#include "HSA_kernel_beamform.hpp"
#include <unistd.h>
#include <string.h>
#include <math.h>

HSA_kernel_beamform::HSA_kernel_beamform(HSA_device *device)
    : HSA_kernel(device)
{
	    /* Load the kernel object. */
    this->kernel_object = this->load_hsaco_file("kernels/unpack_shift_beaform.hsaco", "zero_padded_FFT512");
}

void HSA_kernel_beamform::initialize_memory(){

    //Clamping address
    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->host_region, 256*sizeof(int), 0, (void**)&this->host_map);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    float t, delta_t, Beam_Ref;
    int cl_index;
    float D2R = PI/180.;
    int pad = 2 ;
    
    for (int b=0;b< 256; b++){
      Beam_Ref = asin(light*(b-256/2.) / (Freq_ref*1.e6) / (256) /feed_sep) * 180./ PI;
      t = 256*pad*(Freq_ref*1.e6)*(feed_sep/light*sin(Beam_Ref*D2R)) + 0.5;
      delta_t = 256*pad*(FREQ1*1e6-Freq_ref*1e6) * (feed_sep/light*sin(Beam_Ref*D2R));
      cl_index = (int) floor(t + delta_t) + 256*pad/2.;
      if (cl_index < 0)
	cl_index = 256*pad + cl_index;
      else if (cl_index > 256*pad)
	cl_index = cl_index - 256*pad;
      cl_index = cl_index - 256;
      if (cl_index < 0){
	cl_index = 256*pad + cl_index;
      }
      this->host_map[b] = cl_index;
    }

    //Coefficients for EW brute force beamform
    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->host_region, 32*sizeof(float), 0, (void**)&this->host_coeff);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    for (int angle_iter=0; angle_iter < 4; angle_iter++){
      float anglefrac = sin(0.4*angle_iter*PI/180.);   //say 0, 0.4, 0.8 and 1.2 for now.
      for (int cylinder=0; cylinder < 4; cylinder++){
	this->host_coeff[angle_iter*4*2 + cylinder*2] = cos(2*PI*anglefrac*cylinder*22*FREQ1*1.e6/light);
	this->host_coeff[angle_iter*4*2 + cylinder*2 + 1] = sin(2*PI*anglefrac*cylinder*22*FREQ1*1.e6/light);
      }
    }

    //Output of beamform
    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->host_region, N_ELEM*2*(N_ITER)*sizeof(float), 0, (void**)&this->output_bf);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    memset(this->output_bf,0,N_ELEM*N_ITER*2*sizeof(float));


    //Print to screen for checking
    printf("Input for beamform kernel!\n");
    printf("%d %d %f ", this->input[0], this->host_map[0], this->host_coeff[0]);


    this->sync_copy_host_to_gpu(this->input_buffer, this->input, N_ELEM * N_ITER);
    this->sync_copy_host_to_gpu(this->map_buffer, this->host_map, 256*sizeof(int));
    this->sync_copy_host_to_gpu(this->coeff_buffer, this->host_coeff, 32*sizeof(float));
    //    this->sync_copy_host_to_gpu(this->output_buffer, this->output_bf, N_ELEM*2*N_ITER*sizeof(float));  //Do I need these?
    


}

void HSA_kernel_beamform::allocate_hsa_memory(void **mem_pointers){
    this->gpu_allocate((void**)&this->input_buffer, N_ELEM * N_ITER, mem_pointers[0]);
    this->gpu_allocate((void**)&this->map_buffer, 256*sizeof(int), mem_pointers[1]);
    this->gpu_allocate((void**)&this->coeff_buffer, 32*sizeof(float), mem_pointers[2]);
    this->gpu_allocate((void**)&this->output_buffer, N_ELEM*N_ITER*2*sizeof(float), mem_pointers[3]);

    this->hsa_status = hsa_memory_allocate(device->kernarg_region,64, &this->kernarg_address);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
}

void **HSA_kernel_beamform::get_hsa_memory(){
  return NULL;
}

HSA_kernel_beamform::~HSA_kernel_beamform(){

    this->hsa_status = hsa_memory_free(this->kernarg_address);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

//    this->hsa_status = hsa_amd_memory_pool_free(this->input_buffer);
//    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    this->hsa_status = hsa_amd_memory_pool_free(this->map_buffer);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    this->hsa_status = hsa_amd_memory_pool_free(this->coeff_buffer);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    //    this->hsa_status = hsa_amd_memory_pool_free(this->output_buffer); //Do I need this?
    //assert(this->hsa_status == HSA_STATUS_SUCCESS);

    this->hsa_status = hsa_amd_memory_pool_free(this->output_bf);
    this->hsa_status = hsa_amd_memory_pool_free(this->host_map);
    this->hsa_status = hsa_amd_memory_pool_free(this->host_coeff);

}

void HSA_kernel_beamform::enqueue(hsa_signal_t *signal){
    struct __attribute__ ((aligned(16))) args_t {
        void *input_buffer;
        void *map_buffer;
        void *coeff_buffer;
        void *output_buffer;
    } args;
    memset(&args, 0, sizeof(args));
    args.input_buffer=this->input_buffer;
    args.map_buffer=this->map_buffer;
    args.coeff_buffer=this->coeff_buffer;
    args.output_buffer=this->output_buffer;
    // Allocate the kernel argument buffer from the correct region.
    memcpy(this->kernarg_address, &args, sizeof(args));

    // Obtain the current queue write index.
    uint64_t index = hsa_queue_load_write_index_acquire(this->device->queue);
    hsa_kernel_dispatch_packet_t* dispatch_packet = (hsa_kernel_dispatch_packet_t*)this->device->queue->base_address +
                                                            (index % this->device->queue->size);
    dispatch_packet->setup  |= 3 << HSA_KERNEL_DISPATCH_PACKET_SETUP_DIMENSIONS;
    dispatch_packet->workgroup_size_x = (uint32_t)256;
    dispatch_packet->workgroup_size_y = (uint16_t)1;
    dispatch_packet->workgroup_size_z = (uint16_t)1;
    dispatch_packet->grid_size_x = (uint32_t)256;
    dispatch_packet->grid_size_y = (uint16_t)2;
    dispatch_packet->grid_size_z = (uint32_t)N_ITER;
    dispatch_packet->completion_signal = *signal;
    dispatch_packet->kernel_object = this->kernel_object;
    dispatch_packet->kernarg_address = (void*) this->kernarg_address;
    dispatch_packet->private_segment_size = 0;
    dispatch_packet->group_segment_size = (uint32_t)16384; //Not sure if I need that
    dispatch_packet-> header =
      (HSA_PACKET_TYPE_KERNEL_DISPATCH << HSA_PACKET_HEADER_TYPE) |
      (1 << HSA_PACKET_HEADER_BARRIER) |
      (HSA_FENCE_SCOPE_SYSTEM << HSA_PACKET_HEADER_ACQUIRE_FENCE_SCOPE) |
      (HSA_FENCE_SCOPE_SYSTEM << HSA_PACKET_HEADER_RELEASE_FENCE_SCOPE);

    hsa_queue_add_write_index_acquire(this->device->queue, 1);
    hsa_signal_store_relaxed(this->device->queue->doorbell_signal, index);
}


int HSA_kernel_beamform::copy_output_data(){
    hsa_signal_t sig;
    this->hsa_status = hsa_signal_create(1, 0, NULL, &sig);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);
    hsa_amd_agents_allow_access(1, &this->device->agent, NULL, this->output_bf);
    hsa_amd_agents_allow_access(1, &this->device->cpu_agent, NULL, this->output_buffer);
    this->hsa_status = hsa_amd_memory_async_copy(this->output_bf, this->device->cpu_agent,
                                                 this->output_buffer, this->device->agent,
                                                 N_ELEM*2*N_ITER*sizeof(float), 0, NULL, sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
        hsa_signal_wait_acquire(sig, HSA_SIGNAL_CONDITION_LT, 1, UINT64_MAX, HSA_WAIT_STATE_BLOCKED);
        this->hsa_status=hsa_signal_destroy(sig);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);

    return 0;
}

int HSA_kernel_beamform::verify_output(int spotcheck){
  this->copy_output_data();
  printf("Beamformed data (output of bf kernel):------------------\n");
  for (int i=0;i<3;i++){
    for (int j=0;j<3;j++){
      printf("(%.2f %.2f) ", this->output_bf[(i*(N_ELEM)+j)*2], this->output_bf[(i*(N_ELEM)+j)*2+1]);
    }
    printf("\n");
  }
  printf("...\n");
  for (int i=32760;i<32769;i++){
    for (int j =3;j>0;j--){
      printf("(%.2f %.2f) ", this->output_bf[(i*(2048)-(j))*2], this->output_bf[(i*(2048)-(j))*2+1]);
    }
    printf("\n");
  }
  printf("...\n");
  printf("...\n");
  for (int i=N_ITER-3;i<N_ITER+1;i++){
    for (int j =3;j>0;j--){
      printf("(%.2f %.2f) ", this->output_bf[(i*(2048)-(j))*2], this->output_bf[(i*(2048)-(j))*2+1]);
    }
    printf("\n");
  }


  return 0;
}



