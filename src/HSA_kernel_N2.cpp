#include "HSA_kernel_N2.hpp"
#include <unistd.h>
#include <string.h>

HSA_kernel_N2::HSA_kernel_N2(HSA_device *device)
    : HSA_kernel(device)
{
	    /* Load the kernel object. */
    this->kernel_object = this->load_hsaco_file("kernels/N2.hsaco", "CHIME_X");
}

void HSA_kernel_N2::initialize_memory(){
    int blkid=0;
    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->host_region, N_BLK*2*sizeof(uint32_t), 0, (void**)&this->host_blk_map);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    for (int y=0; blkid<N_BLK; y++)
      for (int x=y; x<N_ELEM/32; x++) {
       this->host_blk_map[2*blkid+0] = x;
       this->host_blk_map[2*blkid+1] = y;
       blkid++;
      }

    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->host_region, N_BLK*32*32*2*sizeof(uint32_t), 0, (void**)&this->output);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    memset(this->output,0,N_BLK*32*32*2*sizeof(uint32_t));

    this->hsa_status=hsa_amd_memory_pool_allocate(this->device->host_region, sizeof(kernel_config_t), 0, (void**)&this->kernel_config);
    this->kernel_config->n_elem=N_ELEM;
    this->kernel_config->n_intg=N_INTG;
    this->kernel_config->n_iter=N_ITER;
    this->kernel_config->n_blk =N_BLK;

    this->sync_copy_host_to_gpu(this->input_buffer, this->input, N_ELEM * N_ITER);
    this->sync_copy_host_to_gpu(this->blk_map, this->host_blk_map, N_BLK*2*sizeof(int));
//    this->sync_copy_host_to_gpu(this->presum_buffer, this->presum, N_ELEM*2*sizeof(int));
    this->sync_copy_host_to_gpu(this->corr_buffer, this->output, N_BLK*32*32*2*sizeof(int));
    this->sync_copy_host_to_gpu(this->cfg_buffer, this->kernel_config, sizeof(kernel_config_t));
}

void HSA_kernel_N2::allocate_hsa_memory(void **mem_pointers){
    this->gpu_allocate((void**)&this->blk_map, N_BLK*2*sizeof(int), mem_pointers[0]);
    this->gpu_allocate((void**)&this->input_buffer, N_ELEM * N_ITER, mem_pointers[1]);
    this->gpu_allocate((void**)&this->presum_buffer, N_ELEM * 2 * sizeof(uint32_t), mem_pointers[2]);
    this->gpu_allocate((void**)&this->corr_buffer, N_BLK*32*32*2*sizeof(int), mem_pointers[3]);
    this->gpu_allocate((void**)&this->cfg_buffer, sizeof(kernel_config_t), mem_pointers[4]);

    this->hsa_status = hsa_memory_allocate(device->kernarg_region,64, &this->kernarg_address);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
}

void **HSA_kernel_N2::get_hsa_memory(){
/*  void *mem[5] = {
    (void*)this->blk_map,
    (void*)this->input_buffer,
    (void*)this->presum_buffer,
    (void*)this->corr_buffer,
    (void*)this->cfg_buffer
  };
  return mem;*/
  return NULL;
}

HSA_kernel_N2::~HSA_kernel_N2(){

    this->hsa_status = hsa_memory_free(this->kernarg_address);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    this->hsa_status = hsa_amd_memory_pool_free(this->cfg_buffer);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    this->hsa_status = hsa_amd_memory_pool_free(this->corr_buffer);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
//    this->hsa_status = hsa_amd_memory_pool_free(this->presum_buffer);
//    assert(this->hsa_status == HSA_STATUS_SUCCESS);
//    this->hsa_status = hsa_amd_memory_pool_free(this->input_buffer);
//    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    this->hsa_status = hsa_amd_memory_pool_free(this->blk_map);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    this->hsa_status = hsa_amd_memory_pool_free(this->output);
    this->hsa_status = hsa_amd_memory_pool_free(this->kernel_config);
}

void HSA_kernel_N2::enqueue(hsa_signal_t *signal){
    struct __attribute__ ((aligned(16))) args_t {
        void *input_buffer;
        void *presum_buffer;
        void *corr_buffer;
        void *blk_map;
        void *config;
    } args;
    memset(&args, 0, sizeof(args));
    args.input_buffer=this->input_buffer;
    args.presum_buffer=this->presum_buffer;
    args.corr_buffer=this->corr_buffer;
    args.blk_map=this->blk_map;
    args.config=this->cfg_buffer;
    // Allocate the kernel argument buffer from the correct region.
    memcpy(this->kernarg_address, &args, sizeof(args));

    // Obtain the current queue write index.
    uint64_t index = hsa_queue_load_write_index_acquire(this->device->queue);
    hsa_kernel_dispatch_packet_t* dispatch_packet = (hsa_kernel_dispatch_packet_t*)this->device->queue->base_address +
                                                            (index % this->device->queue->size);
    dispatch_packet->setup  |= 3 << HSA_KERNEL_DISPATCH_PACKET_SETUP_DIMENSIONS;
    dispatch_packet->workgroup_size_x = (uint16_t)16;
    dispatch_packet->workgroup_size_y = (uint16_t)4;
    dispatch_packet->workgroup_size_z = (uint16_t)1;
    dispatch_packet->grid_size_x = (uint32_t)16;
    dispatch_packet->grid_size_y = (uint32_t)4*N_ITER/N_INTG;
    dispatch_packet->grid_size_z = (uint32_t)N_BLK;
    dispatch_packet->completion_signal = *signal;
    dispatch_packet->kernel_object = kernel_object;
    dispatch_packet->kernarg_address = (void*) kernarg_address;
    dispatch_packet->private_segment_size = 0;
    dispatch_packet->group_segment_size = 0;
//        __atomic_store_n((uint8_t*)(&dispatch_packet->header), (uint8_t)HSA_PACKET_TYPE_KERNEL_DISPATCH, __ATOMIC_RELEASE);
    dispatch_packet-> header =
      (HSA_PACKET_TYPE_KERNEL_DISPATCH << HSA_PACKET_HEADER_TYPE) |
      (1 << HSA_PACKET_HEADER_BARRIER) |
      (HSA_FENCE_SCOPE_SYSTEM << HSA_PACKET_HEADER_ACQUIRE_FENCE_SCOPE) |
      (HSA_FENCE_SCOPE_SYSTEM << HSA_PACKET_HEADER_RELEASE_FENCE_SCOPE);

    hsa_queue_add_write_index_acquire(this->device->queue, 1);
    hsa_signal_store_relaxed(this->device->queue->doorbell_signal, index);
}


int HSA_kernel_N2::copy_output_data(){
    hsa_signal_t sig;
    this->hsa_status = hsa_signal_create(1, 0, NULL, &sig);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);
hsa_amd_agents_allow_access(1, &this->device->agent, NULL, this->output);
hsa_amd_agents_allow_access(1, &this->device->cpu_agent, NULL, this->corr_buffer);
    this->hsa_status = hsa_amd_memory_async_copy(this->output, this->device->cpu_agent,
                                                 this->corr_buffer, this->device->agent,
                                                 N_BLK*32*32*2*sizeof(int), 0, NULL, sig);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
        hsa_signal_wait_acquire(sig, HSA_SIGNAL_CONDITION_LT, 1, UINT64_MAX, HSA_WAIT_STATE_BLOCKED);
        this->hsa_status=hsa_signal_destroy(sig);
        assert(this->hsa_status == HSA_STATUS_SUCCESS);

    return 0;
}

int HSA_kernel_N2::verify_output(int spotcheck){
  this->copy_output_data();
  int errct=0;
  if (spotcheck <= 0) {
    printf("Running full check, could take a while...\n");
    for (int b=0; b<N_BLK; b++){
      for (int y=0; y<32; y++){
        for (int x=0; x<32; x++){
          int re=0;
          int im=0;
          for (int t=0; t<N_ITER; t++){
            int xi = (this->input[this->host_blk_map[2*b+0]*32 + x + t*N_ELEM] & 0x0f)       - 8;
            int xr =((this->input[this->host_blk_map[2*b+0]*32 + x + t*N_ELEM] & 0xf0) >> 4) - 8;
            int yi = (this->input[this->host_blk_map[2*b+1]*32 + y + t*N_ELEM] & 0x0f)       - 8;
            int yr =((this->input[this->host_blk_map[2*b+1]*32 + y + t*N_ELEM] & 0xf0) >> 4) - 8;
            re += xr*yr + xi*yi;
            im += xi*yr - yi*xr;
          }
          if ((this->output[b*32*32*2 + x*2 + y*32*2 + 0] != im) || 
              (this->output[b*32*32*2 + x*2 + y*32*2 + 1] != re)) errct++;
        }
      }
    }
  } else {
    printf("Checking %i correlates at random...\n", spotcheck);
    for (int i=0; i<spotcheck; i++){
      int b=rand() * 1.0 / RAND_MAX * N_BLK;
      int x=rand() * 1.0 / RAND_MAX * 32;
      int y=rand() * 1.0 / RAND_MAX * 32;
      int re=0;
      int im=0;
      for (int t=0; t<N_ITER; t++){
        int xi = (this->input[this->host_blk_map[2*b+0]*32 + x + t*N_ELEM] & 0x0f)       - 8;
        int xr =((this->input[this->host_blk_map[2*b+0]*32 + x + t*N_ELEM] & 0xf0) >> 4) - 8;
        int yi = (this->input[this->host_blk_map[2*b+1]*32 + y + t*N_ELEM] & 0x0f)       - 8;
        int yr =((this->input[this->host_blk_map[2*b+1]*32 + y + t*N_ELEM] & 0xf0) >> 4) - 8;
        re += xr*yr + xi*yi;
        im += xi*yr - yi*xr;
      }
      if ((this->output[b*32*32*2 + x*2 + y*32*2 + 0] != im) || 
          (this->output[b*32*32*2 + x*2 + y*32*2 + 1] != re)) errct++;
    }

  }
  if (errct > 0) printf("   Error! CPU does not match GPU...\n");
  else printf("   Success! CPU matches GPU!\n");

  return errct;
}



