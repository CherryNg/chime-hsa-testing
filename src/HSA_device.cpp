#include "HSA_device.hpp"



hsa_status_t FindCpuDevice(hsa_agent_t agent, void* data) {
  if (data == NULL) {
    return HSA_STATUS_ERROR_INVALID_ARGUMENT;
  }

  hsa_device_type_t hsa_device_type;
  hsa_status_t hsa_error_code =
      hsa_agent_get_info(agent, HSA_AGENT_INFO_DEVICE, &hsa_device_type);
  if (hsa_error_code != HSA_STATUS_SUCCESS) {
    return hsa_error_code;
  }

  if (hsa_device_type == HSA_DEVICE_TYPE_CPU) {
    *((hsa_agent_t*)data) = agent;
    return HSA_STATUS_INFO_BREAK;
  }

  return HSA_STATUS_SUCCESS;
}

HSA_device::HSA_device(int gpu_idx){
    this->gpu_idx = gpu_idx;

    gpu_config_t gpu_config;
    gpu_config.agent = &this->agent;
    gpu_config.gpu_idx = this->gpu_idx;

    gpu_mem_config_t gpu_mem_config;
    gpu_mem_config.region = &this->global_region;
    gpu_mem_config.gpu_idx = this->gpu_idx;

    this->hsa_status = hsa_iterate_agents(FindCpuDevice, &this->cpu_agent);
    if(this->hsa_status == HSA_STATUS_INFO_BREAK) { this->hsa_status = HSA_STATUS_SUCCESS; }
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    hsa_amd_agent_iterate_memory_pools(this->cpu_agent, this->get_device_memory_region, &this->host_region);

//    this->hsa_status = hsa_iterate_agents(this->get_gpu_agent, &this->agent);
    this->hsa_status = hsa_iterate_agents(this->get_gpu_agent, &gpu_config);
    if(this->hsa_status == HSA_STATUS_INFO_BREAK) { this->hsa_status = HSA_STATUS_SUCCESS; }
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    //USE AMD EXTENSIONS TO QUERY CARD CAPABILITIES
    this->hsa_status = hsa_agent_get_info(this->agent, (hsa_agent_info_t)HSA_AMD_AGENT_INFO_COMPUTE_UNIT_COUNT, &this->compute_count);
    this->hsa_status = hsa_agent_get_info(this->agent, (hsa_agent_info_t)HSA_AMD_AGENT_INFO_MAX_CLOCK_FREQUENCY, &this->clock_freq_mhz);
    this->card_tflops = clock_freq_mhz*1e6 * compute_count*16*4*2 / 1e12;

    this->hsa_status = hsa_agent_get_info(this->agent, HSA_AGENT_INFO_NAME, this->agent_name);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    int num;
    this->hsa_status = hsa_agent_get_info(this->agent, HSA_AGENT_INFO_NODE, &num);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

    printf("Initializing %s at idx %i.\n", this->agent_name, num-1);


    this->global_region.handle = (uint64_t)-1;
//    hsa_agent_iterate_regions(this->agent, this->get_device_memory_region, &this->global_region);
    hsa_amd_agent_iterate_memory_pools(this->agent, this->get_device_memory_region, &this->global_region);
    this->hsa_status = (global_region.handle == (uint64_t)-1) ? HSA_STATUS_ERROR : HSA_STATUS_SUCCESS;
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

printf("found memory pool...\n");

    /* Find a memory region that supports kernel arguments.*/
    this->kernarg_region.handle=(uint64_t)-1;
    hsa_agent_iterate_regions(this->agent, this->get_kernarg_memory_region, &this->kernarg_region);
    this->hsa_status = (this->kernarg_region.handle == (uint64_t)-1) ? HSA_STATUS_ERROR : HSA_STATUS_SUCCESS;
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    void* kernarg_address = NULL;


    /* Query the maximum size of the queue.*/
    /* Create a queue using the maximum size. */
    uint32_t queue_size = 0;
    this->hsa_status = hsa_agent_get_info(this->agent, HSA_AGENT_INFO_QUEUE_MAX_SIZE, &queue_size);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
    this->hsa_status = hsa_queue_create(this->agent, queue_size, HSA_QUEUE_TYPE_SINGLE, NULL, NULL, UINT32_MAX, UINT32_MAX, &this->queue);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);

}

HSA_device::~HSA_device(){
    this->hsa_status=hsa_queue_destroy(this->queue);
    assert(this->hsa_status == HSA_STATUS_SUCCESS);
}

float HSA_device::get_card_tflops(void){
    return this->card_tflops;
}

/*
 * Determines if the given agent is of type HSA_DEVICE_TYPE_GPU
 * and sets the value of data to the agent handle if it is.
 */
hsa_status_t HSA_device::get_gpu_agent(hsa_agent_t agent, void *data) {
    hsa_device_type_t device_type;
    hsa_status_t status;
    int num;

    gpu_config_t *gpu_config = (gpu_config_t*)data;

    status = hsa_agent_get_info(agent, HSA_AGENT_INFO_DEVICE, &device_type);
    assert(status == HSA_STATUS_SUCCESS);
    status = hsa_agent_get_info(agent, HSA_AGENT_INFO_NODE, &num);
    assert(status == HSA_STATUS_SUCCESS);
    if ((HSA_DEVICE_TYPE_GPU == device_type) &&
        (gpu_config->gpu_idx == (num-1)))
    {
        *gpu_config->agent = agent;
        return HSA_STATUS_INFO_BREAK;
    }
    return HSA_STATUS_SUCCESS;
}

/*
 * Determines if a memory region can be used for kernarg
 * allocations.
 */
hsa_status_t HSA_device::get_kernarg_memory_region(hsa_region_t region, void* data) {
    hsa_region_segment_t segment;
    hsa_region_get_info(region, HSA_REGION_INFO_SEGMENT, &segment);
    if (HSA_REGION_SEGMENT_GLOBAL != segment) {
        return HSA_STATUS_SUCCESS;
    }

    hsa_region_global_flag_t flags;
    hsa_region_get_info(region, HSA_REGION_INFO_GLOBAL_FLAGS, &flags);
    if (flags & HSA_REGION_GLOBAL_FLAG_KERNARG) {
        hsa_region_t* ret = (hsa_region_t*) data;
        *ret = region;
        return HSA_STATUS_INFO_BREAK;
    }

    return HSA_STATUS_SUCCESS;
}


/* Determines if a memory region can be used for device 
 * allocations.  On APU this is host memory, on a dGPU 
 * this is the coarse-grained non-system memory:      */
hsa_status_t HSA_device::get_device_memory_region(hsa_amd_memory_pool_t region, void* data) {
    hsa_amd_segment_t segment;
    hsa_amd_memory_pool_get_info(region, HSA_AMD_MEMORY_POOL_INFO_SEGMENT, &segment);
    if (HSA_AMD_SEGMENT_GLOBAL != segment) {
        return HSA_STATUS_SUCCESS;
    }

    hsa_amd_memory_pool_global_flag_t flags;
    hsa_amd_memory_pool_get_info(region, HSA_AMD_MEMORY_POOL_INFO_GLOBAL_FLAGS, &flags);
    if ((flags & HSA_AMD_MEMORY_POOL_GLOBAL_FLAG_FINE_GRAINED) ||
        (flags & HSA_AMD_MEMORY_POOL_GLOBAL_FLAG_COARSE_GRAINED)) 
    {
        printf ( "found device region, flags=%x\n", flags);
        hsa_amd_memory_pool_t* ret = (hsa_amd_memory_pool_t*) data;
        *ret = region;
        return HSA_STATUS_INFO_BREAK;
    }
/*
    hsa_region_global_flag_t flags;
    hsa_region_get_info(region, HSA_REGION_INFO_GLOBAL_FLAGS, &flags);
    if ((flags & HSA_REGION_GLOBAL_FLAG_FINE_GRAINED) ||
        (flags & HSA_REGION_GLOBAL_FLAG_COARSE_GRAINED)) 
    {
        printf ( "found device region, flags=%x\n", flags);
//        hsa_region_t* ret = (hsa_region_t*) data;
//        *ret = region;
        *gpu_mem_config->region = region;
        return HSA_STATUS_INFO_BREAK;
    }
*/
    return HSA_STATUS_SUCCESS;
}
