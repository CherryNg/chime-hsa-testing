#pragma once
#include "HSA_kernel.hpp"
#define N_ELEM 2048
#define N_ITER 38400 // 32768
#define Freq_ref 492.125984252
#define light 3.e8
#define feed_sep 0.3048
#define PI 3.14159265
#define FREQ1 450.0

class HSA_kernel_beamform : public HSA_kernel {
	public:
		HSA_kernel_beamform(HSA_device *device);
		~HSA_kernel_beamform();
		void enqueue(hsa_signal_t*);

		int copy_output_data();
		int verify_output(int spotcheck = 0);
		void allocate_hsa_memory(void **);
		void initialize_memory();
		void **get_hsa_memory();

                unsigned char *input;
                float *output_bf;
                uint32_t *host_map;
                float *host_coeff;

	    unsigned int* input_buffer;
	    unsigned int* output_buffer;
            unsigned int* map_buffer;
            unsigned int* coeff_buffer;
	    void *kernarg_address;
};
